
## _Deprecation Notice_
This Pre-Built has been deprecated as of 07-12-2024 and will be end of life on 07-12-2025. The capabilities of this Pre-Built have been replaced by [Cisco - IOS - IAG](https://gitlab.com/itentialopensource/pre-built-automations/cisco-ios-iag).

<!-- Update the below line with your artifact name -->
# Artifact Name

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

The Port Turn Up Pre-Built consists of several workflows and command templates meant to serve as a solution for performing a port turn up on Cisco routers running IOS software.
In this Pre-Built, we are using Itential Automation Gateway (IAG) as the network orchestrator.
The included pre and post checks should be customized to suit the needs of the target device. Additionally, the implementation of routing network traffic is left up to the user to provide. The Pre-Built provides an indication of where a failover and restore should take place, but assumes no responsibility for these tasks.
<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
The Pre-Built consist of following:
  - Automation Catalog(IAP Artifacts Port Turn Up Ansible)
  - WorkFlow(IAP Artifacts Port Turn Up Ansible)
  - WorkFlow(IAP Artifacts Port Turn Up Ansible Deployment)
  - Command Template(IAP-Artifacts Port Turn Up Post-Check)
  - Command Template(IAP-Artifacts Port Turn Up Pre-Check)


<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

## Installation Prerequisites
In order to use the Port Turn Up Pre-Built, users will have to satisfy the following pre-requisites:

* Itential Automation Platform
  * `^2023.1.x`
* Itential Automation Gateway
  * `^3.227.0+2023.1.15`

## Requirements
 * Cisco IOS Device

## Features

* Automatically checks for device type
* Displays dry-run to user (asking for confirmation) prior to pushing config to the device
* Turns up interface and changes IpAddress and Description 

<!-- Unordered list highlighting the most exciting features of the artifact -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->

## Future Enhancements
 * Provide support for different device operation system types like, IOSXR, Arista, F5, Nokia and etc

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED ARTIFACT -->

Alternatively, you may clone this repository and run `npm pack` to create a tarball which can then be installed via the offline installer in App-Artifacts. Please consult the documentation for App-Artifacts for further information.

<!-- OPTIONAL - Explain if external components are required outside of IAP. Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->
Use the following to run this artifact: 

This Pre-Built may be run from Operations Manager by clicking `Run` for the IAP Pre-Built Port Turn UP JSON Form. The user will be presented with a form to select options customize which device type, interface type, IP Address and subnet mask. A description is requred to run this artifact. The shutdown option is to turn down the device.

## Additional Information

Please use your Itential Customer Success account if you need support when using this artifact.
