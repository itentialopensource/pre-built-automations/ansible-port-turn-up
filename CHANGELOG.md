
## 0.0.9 [07-15-2024]

Adds deprecation and metadata and removes image from README

See merge request itentialopensource/pre-built-automations/ansible-port-turn-up!11

2024-07-15 13:48:11 +0000

---

## 0.0.8 [05-30-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/ansible-port-turn-up!8

---

## 0.0.7 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/ansible-port-turn-up!7

---

## 0.0.6 [10-21-2021]

* Patch/dsup 958

See merge request itentialopensource/pre-built-automations/ansible-port-turn-up!5

---

## 0.0.5 [10-05-2021]

* Patch/dsup 1068

See merge request itentialopensource/pre-built-automations/ansible-port-turn-up!4

---

## 0.0.4 [01-11-2021]

* Update  IAP Artifacts Port Turn Up Ansible Deployment.json

See merge request itentialopensource/pre-built-automations/staging/ansible-port-turn-up!2

---

## 0.0.3 [01-07-2021]

* Patch/lb 520

See merge request itentialopensource/pre-built-automations/staging/ansible-port-turn-up!1

---

## 0.0.2 [01-07-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n
